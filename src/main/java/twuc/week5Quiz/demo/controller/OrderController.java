package twuc.week5Quiz.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import twuc.week5Quiz.demo.domain.OrderProduct;

@RestController
@RequestMapping("/api")
public class OrderController {

    @PostMapping("/orders")
    public ResponseEntity createOrder(@RequestBody OrderProduct orderProduct) {
        return ResponseEntity.status(201).build();
    }
}
