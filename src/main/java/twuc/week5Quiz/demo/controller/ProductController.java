package twuc.week5Quiz.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import twuc.week5Quiz.demo.domain.Product;
import twuc.week5Quiz.demo.domain.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @PostMapping("/products")
    public ResponseEntity createProduct(@RequestBody Product product){
        productRepository.save(product);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> products = productRepository.findAll();
        return ResponseEntity.status(200)
                .header("kinds of produccts", String.valueOf(products.size()))
                .body(products);
    }


}



