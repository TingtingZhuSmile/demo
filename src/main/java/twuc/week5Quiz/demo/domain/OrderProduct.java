package twuc.week5Quiz.demo.domain;

import javax.persistence.*;

public class OrderProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private int amount;

    public OrderProduct() {

    }

    public OrderProduct(int amount) {
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }
}
