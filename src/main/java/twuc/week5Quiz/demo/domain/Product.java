package twuc.week5Quiz.demo.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false, length = 128)
    private String productName;

    @NotNull
    @Column(nullable = false)
    private int price;

    @NotNull
    @Column(nullable = false, length = 128)
    private String unit;

    @NotNull
    @Column(nullable = false, length = 128)
    private String url;

    public Product() {

    }

    public Product(@NotNull String name, @NotNull int price, @NotNull String unit, @NotNull String url) {
        this.productName = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
