create table if not exists products(
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    productName VARCHAR(128) not null,
    price INT not null,
    unit varchar(128) not null,
    url varchar(128) not null
)default charset= utf8;

