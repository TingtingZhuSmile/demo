package twuc.week5Quiz.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import twuc.week5Quiz.demo.domain.Product;
import twuc.week5Quiz.demo.domain.ProductRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext
class DemoApplicationTests {

	@Autowired
	MockMvc mockMvc;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    EntityManager entityManager;

    private List<Long> createProducts(List<Product> products) {
        List<Long> ids = new ArrayList<>();

        productRepository.saveAll(products);
        productRepository.flush();
        ids.addAll(products.stream().map(Product::getId).collect(Collectors.toList()));
        entityManager.clear();//可有可无
        return ids;
    }

	@Test
	void should_add_product() throws Exception {
		Product product = new Product("Coco", 3, "瓶", "www.baidu.com");
		String productStr = new ObjectMapper().writeValueAsString(product);
		mockMvc.perform(post("/api/products")
                .contentType("application/json")
                .content(productStr))
                .andExpect(status().is(201));

	}

    @Test
    void should_get_products() throws Exception {
        createProducts(Arrays.asList(
                new Product("Coco1", 1, "瓶", "www.baidu.com"),
                new Product("Coco2", 2, "瓶", "www.baidu.com1")));

        String contentJson = mockMvc.perform(get("/api/products"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        Product[] products = new ObjectMapper().readValue(contentJson, Product[].class);

        assertEquals(2, products.length);
        assertEquals(1L,products[0].getId().byteValue());
        assertEquals("瓶",products[0].getUnit());
        assertEquals(1,products[0].getPrice());
        assertEquals("www.baidu.com",products[0].getUrl());


    }
}
