package twuc.week5Quiz.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import twuc.week5Quiz.demo.domain.OrderProduct;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@DirtiesContext
@AutoConfigureMockMvc
public class OrderTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_add_order() throws Exception {
        OrderProduct orderProduct = new OrderProduct(2);
        mockMvc.perform(post("/api/orders/")
                .contentType("application/json")
                .content(new ObjectMapper().writeValueAsString(orderProduct)))
                .andExpect(status().is(201));
    }
}
